package com.javacodegeeks.android.bluetoothtest;

import android.bluetooth.BluetoothSocket;
import android.os.Bundle;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Set;
import java.util.UUID;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivity extends Activity {

   private static final int REQUEST_ENABLE_BT = 1;
   private Button onBtn;
   private Button offBtn;
	private Button  sendButton;
	private Button disconnectButton;
   private Button findBtn;
   private TextView text;
	private TextView statText;
	private TextView sendText;
	private TextView recText;
   private BluetoothAdapter myBluetoothAdapter;
   private Set<BluetoothDevice> pairedDevices;
   private ListView myListView;
   private ArrayAdapter<String> BTArrayAdapter;
	private ArrayAdapter<BluetoothDevice> btDevices;
	private OutputStream outputStream;
	private InputStream inStream;
	private BluetoothSocket connectSocket;
	private ToggleButton bluetoothOnOff;
	private byte[] readBuffer;
	private int readBufferPosition;
	private int counter;
	private volatile boolean stopWorker;
	private String data;

	private static final UUID MY_UUID = UUID.fromString("00000000-0000-0000-0000-000000000000");
   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
 		initMainActivity();
	  }

	public void initMainActivity(){
		setContentView(R.layout.activity_main);

		// take an instance of BluetoothAdapter - Bluetooth radio
		myBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		if(myBluetoothAdapter == null) {
			onBtn.setEnabled(false);
			offBtn.setEnabled(false);

			findBtn.setEnabled(false);
			text.setText("Status: not supported");

			Toast.makeText(getApplicationContext(),"Your device does not support Bluetooth",
					Toast.LENGTH_LONG).show();
		} else {
			text = (TextView) findViewById(R.id.text);



			findBtn=(Button) findViewById(R.id.search);

			findBtn.setOnClickListener(new  OnClickListener() {

										   @Override
										   public void onClick (View v){
											   // TODO Auto-generated method stub
											   find(v);
										   }
									   }

			);

			myListView=(ListView) findViewById(R.id.listView1);

			myListView.setOnItemClickListener(myHandlerScList);
			// create the arrayAdapter that contains the BTDevices, and set it to the ListView
			BTArrayAdapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1);
			myListView.setAdapter(BTArrayAdapter);

			btDevices = new ArrayAdapter<BluetoothDevice>(getApplicationContext(),android.R.layout.simple_list_item_1);
		}
		bluetoothOnOff= (ToggleButton) findViewById(R.id.toggleButton);
		if (myBluetoothAdapter.isEnabled()){
			bluetoothOnOff.setChecked(true);
			bluetoothOnOff.setText("On");
		}
		else
		{
			bluetoothOnOff.setChecked(false);
			bluetoothOnOff.setText("Off");
		}
		bluetoothOnOff.setOnClickListener(new OnClickListener() {
											  @Override
											  public void onClick(View v) {
												  // TODO Auto-generated method stub
												  if (myBluetoothAdapter.isEnabled())
												  {
													  off(v);
													  bluetoothOnOff.setText("Off");
												  }
												  else
												  {
													  on(v);
													  bluetoothOnOff.setText("On");
												  }
											  }
										  }

		);

	}

	public void initChatActivity(){
		setContentView(R.layout.chat_activity);
		recText = (TextView)findViewById(R.id.receiveText);
		sendText = (TextView) findViewById(R.id.sendText);
		statText = (TextView) findViewById(R.id.textConnectedwith);
		sendButton=(Button) findViewById(R.id.button2);
		sendButton.setOnClickListener(new OnClickListener() {
										  @Override
										  public void onClick(View v) {
											  // TODO Auto-generated method stub
											  sendMessage(sendText.getText().toString());
										  }
									  }

		);
		disconnectButton=(Button) findViewById(R.id.disconnectButton);
		disconnectButton.setOnClickListener(new OnClickListener() {
												@Override
												public void onClick(View v) {
													// TODO Auto-generated method stub
													disconnect();
												}
											}

		);


	}

   public void on(View view){
      if (!myBluetoothAdapter.isEnabled()) {
         Intent turnOnIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
         startActivityForResult(turnOnIntent, REQUEST_ENABLE_BT);

         Toast.makeText(getApplicationContext(),"Bluetooth turned on" ,
        		 Toast.LENGTH_LONG).show();
      }
      else{
         Toast.makeText(getApplicationContext(),"Bluetooth is already on",
        		 Toast.LENGTH_LONG).show();
      }
   }
   
   @Override
   protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	   // TODO Auto-generated method stub
	   if(requestCode == REQUEST_ENABLE_BT){
		   if(myBluetoothAdapter.isEnabled()) {
			   text.setText("Status: Enabled");
		   } else {   
			   text.setText("Status: Disabled");
		   }
	   }
   }


	public void sendMessage(String s){
		try {
			outputStream.write(s.getBytes());
			sendText.setText("");
			Toast.makeText(getApplicationContext(),"Message sent",
					Toast.LENGTH_LONG).show();
		} catch (IOException e) {
			e.printStackTrace();
			Toast.makeText(getApplicationContext(),"Message not sent",
					Toast.LENGTH_LONG).show();
		}
	}

   final BroadcastReceiver bReceiver = new BroadcastReceiver() {
	    public void onReceive(Context context, Intent intent) {
	        String action = intent.getAction();
	        // When discovery finds a device
	        if (BluetoothDevice.ACTION_FOUND.equals(action)) {
	             // Get the BluetoothDevice object from the Intent
	        	 BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
	        	 // add the name and the MAC address of the object to the arrayAdapter
	             BTArrayAdapter.add(device.getName() + "\n" + device.getAddress());
	             BTArrayAdapter.notifyDataSetChanged();
				 btDevices.add(device);
	        }
	    }
	};

   public void find(View view) {
	   if (myBluetoothAdapter.isDiscovering()) {
		   // the button is pressed when it discovers, so cancel the discovery
		   myBluetoothAdapter.cancelDiscovery();
	   }
	   else {
			BTArrayAdapter.clear();
			myBluetoothAdapter.startDiscovery();
			
			registerReceiver(bReceiver, new IntentFilter(BluetoothDevice.ACTION_FOUND));	
		}    
   }
   
   public void off(View view){
	  myBluetoothAdapter.disable();
	  text.setText("Status: Disconnected");
	  
      Toast.makeText(getApplicationContext(),"Bluetooth turned off",
    		  Toast.LENGTH_LONG).show();
   }
   
   @Override
   protected void onDestroy() {
	   // TODO Auto-generated method stub
	   super.onDestroy();
	   unregisterReceiver(bReceiver);
   }

	ListView.OnItemClickListener myHandlerScList = (new ListView.OnItemClickListener(){
		public void onItemClick(AdapterView<?> parent, View view, int position,
								long id) {


			BluetoothDevice device = btDevices.getItem(position);



			Method m = null;
			try {
				m = device.getClass().getMethod("createRfcommSocket", new Class[]{int.class});
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			}

// using port#1 / channel #1
			try {
				connectSocket = (BluetoothSocket)m.invoke(device, Integer.valueOf(30));
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}

			try {
				connectSocket.connect();
				outputStream = connectSocket.getOutputStream();
				inStream = connectSocket.getInputStream();

				initChatActivity();

				final Handler handler = new Handler();
				final byte delimiter = 10; //This is the ASCII code for a newline character

				stopWorker = false;
				readBufferPosition = 0;
				readBuffer = new byte[1024];

				new Thread(new Runnable() {
					public void run() {
						while(!Thread.currentThread().isInterrupted() && !stopWorker) {
							int bytesAvailable = 0;
							try {
								bytesAvailable = inStream.available();
							} catch (IOException e) {
								e.printStackTrace();
							}

							if (bytesAvailable > 0) {
                                byte[] packetBytes = new byte[bytesAvailable];

								try {
									inStream.read(packetBytes);
								} catch (IOException e) {
									e.printStackTrace();
								}

								for (int i = 0; i < bytesAvailable; i++) {
                                    byte b = packetBytes[i];
                                    if (b == delimiter) {
                                        byte[] encodedBytes = new byte[readBufferPosition];
                                        System.arraycopy(readBuffer, 0, encodedBytes, 0, encodedBytes.length);

										try {
											data = new String(encodedBytes, "US-ASCII");
										} catch (UnsupportedEncodingException e) {
											e.printStackTrace();
										}

										readBufferPosition = 0;

										handler.post(new Runnable()
										{
											public void run()
											{
												recText.setText(data);
											}
										});
									}
									else
									{
										readBuffer[readBufferPosition++] = b;
									}
								}
							}
						}

					}

				}).start();

				Toast toast = Toast.makeText(getApplicationContext(),"CONNECTED",Toast.LENGTH_SHORT);
				toast.show();

				statText.setText("Connected with " + device.getName());

			} catch (IOException e) {
				e.printStackTrace();
				Toast toast = Toast.makeText(getApplicationContext(),"NOT CONNECTED",Toast.LENGTH_SHORT);
				toast.show();
			}

		}
	});

	public void disconnect(){

		if (connectSocket != null) {
			try {connectSocket.close();} catch (Exception e) {}
			connectSocket = null;
		}
		stopWorker=true;
		initMainActivity();

		Toast.makeText(getApplicationContext(),"Disconnected",
				Toast.LENGTH_LONG).show();
	}
}
