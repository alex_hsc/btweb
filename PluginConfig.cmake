#/**********************************************************\ 
#
# Auto-Generated Plugin Configuration file
# for Bluetooth For Web
#
#\**********************************************************/

set(PLUGIN_NAME "btweb")
set(PLUGIN_PREFIX "BFW")
set(COMPANY_NAME "HSCoburg")

# ActiveX constants:
set(FBTYPELIB_NAME btwebLib)
set(FBTYPELIB_DESC "btweb 1.0 Type Library")
set(IFBControl_DESC "btweb Control Interface")
set(FBControl_DESC "btweb Control Class")
set(IFBComJavascriptObject_DESC "btweb IComJavascriptObject Interface")
set(FBComJavascriptObject_DESC "btweb ComJavascriptObject Class")
set(IFBComEventSource_DESC "btweb IFBComEventSource Interface")
set(AXVERSION_NUM "1")

# NOTE: THESE GUIDS *MUST* BE UNIQUE TO YOUR PLUGIN/ACTIVEX CONTROL!  YES, ALL OF THEM!
set(FBTYPELIB_GUID adeafb3a-eed4-51ca-87f4-b8aafdfba783)
set(IFBControl_GUID c7a2eff0-d6ec-5cc6-bfee-f8b2b64b6d58)
set(FBControl_GUID ee23881a-99c9-56df-999c-87aa9ef1ab69)
set(IFBComJavascriptObject_GUID fb22e0ff-befa-5a3f-8509-bd7f40a80dc1)
set(FBComJavascriptObject_GUID eb53622d-2dc0-56ab-bbc7-6333730795cb)
set(IFBComEventSource_GUID a6747e18-ea8d-5b9e-b566-f38cd3ec76ad)
if ( FB_PLATFORM_ARCH_32 )
    set(FBControl_WixUpgradeCode_GUID d5074e55-db1c-587b-87b6-5d32d58d6169)
else ( FB_PLATFORM_ARCH_32 )
    set(FBControl_WixUpgradeCode_GUID 1ae752b1-b4cc-5efa-b9c5-5e0d676caf98)
endif ( FB_PLATFORM_ARCH_32 )

# these are the pieces that are relevant to using it from Javascript
set(ACTIVEX_PROGID "HSCoburg.btweb")
if ( FB_PLATFORM_ARCH_32 )
    set(MOZILLA_PLUGINID "hs-coburg.de/btweb")  # No 32bit postfix to maintain backward compatability.
else ( FB_PLATFORM_ARCH_32 )
    set(MOZILLA_PLUGINID "hs-coburg.de/btweb_${FB_PLATFORM_ARCH_NAME}")
endif ( FB_PLATFORM_ARCH_32 )

# strings
set(FBSTRING_CompanyName "HSCoburg")
set(FBSTRING_PluginDescription "Enables your favorite website to access your bluetooth devices.")
set(FBSTRING_PLUGIN_VERSION "1.0.0.0")
set(FBSTRING_LegalCopyright "Copyright 2015 HSCoburg")
set(FBSTRING_PluginFileName "np${PLUGIN_NAME}")
if (APPLE)
    # On apple, np is not needed
    set(FBSTRING_PluginFileName "${PLUGIN_NAME}")
endif()
set(FBSTRING_ProductName "Bluetooth For Web")
set(FBSTRING_FileExtents "")
if ( FB_PLATFORM_ARCH_32 )
    set(FBSTRING_PluginName "Bluetooth For Web")  # No 32bit postfix to maintain backward compatability.
else ( FB_PLATFORM_ARCH_32 )
    set(FBSTRING_PluginName "Bluetooth For Web_${FB_PLATFORM_ARCH_NAME}")
endif ( FB_PLATFORM_ARCH_32 )
set(FBSTRING_MIMEType "application/x-btweb")

# Uncomment this next line if you're not planning on your plugin doing
# any drawing:

#set (FB_GUI_DISABLED 1)

# Mac plugin settings. If your plugin does not draw, set these all to 0
set(FBMAC_USE_QUICKDRAW 0)
set(FBMAC_USE_CARBON 1)
set(FBMAC_USE_COCOA 1)
set(FBMAC_USE_COREGRAPHICS 1)
set(FBMAC_USE_COREANIMATION 0)
set(FBMAC_USE_INVALIDATINGCOREANIMATION 0)

# If you want to register per-machine on Windows, uncomment this line
#set (FB_ATLREG_MACHINEWIDE 1)
