/**********************************************************\

Auto-generated btwebAPI.cpp

\**********************************************************/

#include "JSObject.h"
#include "variant_list.h"
#include "DOM/Document.h"
#include "global/config.h"
#include "btwebAPI.h"
#include <winsock2.h>
#include <chrono>
#include <vector>
#include <algorithm>
#include <ws2bth.h>
#include <BluetoothAPIs.h>
#include <iostream>     
#include <boost/thread.hpp> 
#include <boost/format.hpp>
#include <stdio.h>
#include <atlbase.h>
#include <atlconv.h>
#include <string>
#include <cstdio>

#pragma comment(lib, "Ws2_32.lib")

typedef ULONGLONG bt_addr, *pbt_addr, BT_ADDR, *PBT_ADDR;

#define DEFAULT_BUFLEN 512
#define MAX_NAME 248

WSADATA wsd;
SOCKET s;
SOCKET sockets[5];
int count_sockets=0;
SOCKADDR_BTH sockaddrs[5];
SOCKADDR_BTH sab;
bool initListen = true;
int iResult;
char recvbuf[DEFAULT_BUFLEN] = "";
int recvbuflen = DEFAULT_BUFLEN;
std::string strIn;


union {

	// memory for returned struct
	CHAR buf[5000];

	// buffer for BT_ADDR requirements
	SOCKADDR_BTH _Unused_;

} butuh;



///////////////////////////////////////////////////////////////////////////////
/// @fn FB::variant btwebAPI::echo(const FB::variant& msg)
///
/// @brief  Echos whatever is passed from Javascript.
///         Go ahead and change it. See what happens!
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// @fn btwebPtr btwebAPI::getPlugin()
///
/// @brief  Gets a reference to the plugin that was passed in when the object
///         was created.  If the plugin has already been released then this
///         will throw a FB::script_error that will be translated into a
///         javascript exception in the page.
///////////////////////////////////////////////////////////////////////////////
btwebPtr btwebAPI::getPlugin()
{
	btwebPtr plugin(m_plugin.lock());
	if (!plugin) {
		throw FB::script_error("The plugin is invalid");
	}
	return plugin;
}

/*
 * Method: startDiscovery
 * ----------------------------
 *   Starts the discovery for Bluetooth Devices Thread
 *
 *   returns: boolean value for error handling
 */
bool btwebAPI::startDiscovery()
{
	boost::thread t(boost::bind(&btwebAPI::startDiscovery_thread, this));
	m_DiscoveryThread = &t;
	return true;
}

/*
 * Method: stopDiscovery
 * ----------------------------
 *   Stops the discovery for Bluetooth Devices Thread
 */
void btwebAPI::stopDiscovery()
{
	m_stopDiscovery = true;
}

/*
 * Method: startDiscovery_thread
 * ----------------------------
 *   Discovery for Bluetooth Devices Thread
 */
void btwebAPI::startDiscovery_thread()
{
	m_stopDiscovery = false;
	std::vector<ULONGLONG> foundDevices;

	//The WSAStartup function initiates use of the Winsock DLL by a process
	if (WSAStartup(MAKEWORD(2, 2), &wsd) != 0)
	{
		fire_Error("startDiscovery_thread", "WSAStartup fails");
		return;
	}

	WSAQUERYSET wsaq;
	HANDLE hLookup;
	LPWSAQUERYSET pwsaResults;
	DWORD dwSize;
	BOOL bHaveName;
	BT_ADDR btAddr;
	DWORD dwNameSpace;
	pwsaResults = (LPWSAQUERYSET)butuh.buf;
	dwSize = sizeof(butuh.buf);
	ZeroMemory(&wsaq, sizeof(wsaq));
	wsaq.dwSize = sizeof(wsaq);
	wsaq.dwNameSpace = NS_BTH;
	wsaq.lpcsaBuffer = NULL;

	// initialize searching procedure
	for (;;)
	{

		if (m_stopDiscovery)	//set in Method "stopDiscovery"
		{
			return;
		}
		else
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(500));
		}

		/*The WSALookupServiceBegin function initiates a client query that is constrained 
		by the information contained within a WSAQUERYSET structure. WSALookupServiceBegin 
		only returns a handle, which should be used by subsequent calls to 
		WSALookupServiceNext to get the actual results*/
		if (WSALookupServiceBegin(&wsaq, LUP_CONTAINERS, &hLookup) == SOCKET_ERROR)
		{
			continue;
		}

		ZeroMemory(pwsaResults, sizeof(WSAQUERYSET));
		pwsaResults->dwSize = sizeof(WSAQUERYSET);
		pwsaResults->dwNameSpace = NS_BTH;
		pwsaResults->lpBlob = NULL;

		/* The WSALookupServiceNext function is called after obtaining a handle from a 
		previous call to WSALookupServiceBegin in order to retrieve the requested 
		service information. */
		while (WSALookupServiceNext(hLookup, LUP_RETURN_NAME | LUP_RETURN_ADDR, &dwSize, pwsaResults) == 0)
		{

			btAddr = ((SOCKADDR_BTH *)pwsaResults->lpcsaBuffer->RemoteAddr.lpSockaddr)->btAddr; //get Bluetooth Address

			if (std::find(foundDevices.begin(), foundDevices.end(), btAddr) != foundDevices.end())
				continue;
			else
				foundDevices.push_back(btAddr);


			bHaveName = (pwsaResults->lpszServiceInstanceName) && *(pwsaResults->lpszServiceInstanceName);
			dwNameSpace = pwsaResults->dwNameSpace;

			// retrieve remote bt address from results
			CSADDR_INFO *pCSAddr;
			pCSAddr = (CSADDR_INFO *)pwsaResults->lpcsaBuffer;
			char addressAsString[2000];
			DWORD addressSize;
			addressSize = sizeof(addressAsString);

			WSAAddressToString(pCSAddr->RemoteAddr.lpSockaddr, pCSAddr->RemoteAddr.iSockaddrLength, NULL, (LPWSTR)addressAsString, &addressSize);
			std::string macAddress(addressAsString, addressSize);

			// fix: WSAAddressToString returns a '\0' after every character, so we strip them from string
			for (int i = 0; i < addressSize / 2; ++i)
				macAddress.erase(std::find(macAddress.begin(), macAddress.end(), '\0'));

			// fix: Strip brackets from string
			macAddress.erase(std::find(macAddress.begin(), macAddress.end(), '('));
			macAddress.erase(std::find(macAddress.begin(), macAddress.end(), ')'));


			//results[macAddress] = pwsaResults->lpszServiceInstanceName;
			std::string displayName;
			LPWSTR2String(displayName, pwsaResults->lpszServiceInstanceName);
			fire_newDevice(macAddress, displayName);
		}
		/* The WSALookupServiceEnd function is called to free the handle 
		after previous calls to WSALookupServiceBegin and WSALookupServiceNext */
		WSALookupServiceEnd(hLookup);
	}
}

/*
 * Method: send_message_to_device
 * ----------------------------
 *   sends message to connected Bluetooth Device
 *
 *	 message: message string that should be sent
 *
 *   returns: integer value for error handling
 */
int btwebAPI::send_message_to_device(std::string message)
{
	const char *sendbuf = message.c_str();

	int recvbuflen = DEFAULT_BUFLEN;

	char recvbuf[DEFAULT_BUFLEN] = "";

	// Send some data

	iResult = send(s, sendbuf, (int)strlen(sendbuf), 0);

	if (iResult == SOCKET_ERROR) 
	{
		for (int x = 0; x < 5; x++)
		{
			iResult = send(sockets[x], sendbuf, (int)strlen(sendbuf), 0);
		}
	}
	return 0;
}

/*
 * Method: connect_to_device
 * ----------------------------
 *   Starts the connect_to_device_Thread
 *	 
 *	 macAddress: MAC Address of the Bluetooth Device
 *	 deviceName: Name of the Bluetooth Device
 */
void btwebAPI::connect_to_device(std::string macAddress, std::string deviceName)
{
	boost::thread workerThread(&btwebAPI::connect_to_device_Thread, this, macAddress, deviceName);
}

/*
 * Method: connect_to_device_Thread
 * ----------------------------
 *   Connects to a Bluetooth Device
 *
 *	 macAddress: MAC Address of the Bluetooth Device
 *	 deviceName: Name of the Bluetooth Device
 */
void btwebAPI::connect_to_device_Thread(std::string macAddress, std::string deviceName) // mac address format: XX:XX:XX:XX:XX:XX
{

	// formatted, readable mac address to bt_addr
	fire_connectionState(0);
	SOCKADDR_BTH sa = { 0 };

	int sa_len = sizeof(sa);

	wchar_t wtext[20];

	mbstowcs(wtext, macAddress.c_str(), strlen(macAddress.c_str()) + 1);
	
	LPWSTR ptr = wtext;

	WSAStringToAddress(ptr, AF_BTH, NULL, (LPSOCKADDR)&sa, &sa_len);

	BT_ADDR aSddr = sa.btAddr;

	//The WSAStartup function initiates use of the Winsock DLL by a process
	if (WSAStartup(MAKEWORD(2, 2), &wsd) != 0)

	{
		fire_connectionState(-1);
		fire_Error("connect_to_device_Thread", "WSAStartup fails!");
		return;
	}

	// Creates Socket for connection
	s = socket(AF_BTH, SOCK_STREAM, BTHPROTO_RFCOMM);

	if (s == INVALID_SOCKET)

	{
		WSACleanup();	//The WSACleanup function terminates use of the Winsock 2 DLL (Ws2_32.dll)
		fire_connectionState(-1);
		fire_Error("connect_to_device_Thread", "Socket creation fails!");
		return;
	}
	fire_connectionState(50);
	
	memset(&sab, 0, sizeof(sab));

	sab.addressFamily = AF_BTH;

	sab.btAddr = aSddr;

	sab.port = 30;
	
	// Connect to the Bluetooth socket, created previously
	if (connect(s, (SOCKADDR *)&sab, sizeof(sab)) == SOCKET_ERROR)
	{
		closesocket(s);
		WSACleanup();	//The WSACleanup function terminates use of the Winsock 2 DLL (Ws2_32.dll)
		fire_connectionState(-1);
		fire_Error("connect_to_device_Thread", "Connection fails!");
		return;
	}
	
	fire_connectedDevice(macAddress, deviceName);
	fire_connectionState(100);
	return;
}

/*
 * Method: disconnect_from_device
 * ----------------------------
 *   Disconnects from the Bluetooth Device
 *
 *   returns: integer value for error handling
 */
int btwebAPI::disconnect_from_device()
{
	// shutdown the stream connection since no more data will be sent
	// Do all the cleanup

	if (closesocket(s) != 0)
		return -1;

	CloseHandle((LPVOID)s);

	if (WSACleanup() != 0)
		return -1;

	return 0;
}

/*
 * Method: start_listening_for_connection
 * ----------------------------
 *   Starts the listen_for_connection_Thread 
 */
void btwebAPI::start_listening_for_connection()
{
	boost::thread workerThread(&btwebAPI::listen_for_connection_Thread, this);
}

/*
 * Method: stop_listening_for_connection
 * ----------------------------
 *   Stops the listen_for_connection_Thread 
 */
void btwebAPI::stop_listening_for_connection()
{
	closesocket(s);
	for (int x = 0; x < count_sockets; x++)
	{
		closesocket(sockets[x]);
	}
	WSACleanup();
}

/*
 * Method: start_listening_for_message
 * ----------------------------
 *   Starts five Threads for every Socket to listen for messages
 */
void btwebAPI::start_listening_for_message()
{
	for (int x = 0; x < 5; x++)
	{
		boost::thread workerThread(&btwebAPI::listen_for_message_Thread, this, x);
	}
}

/*
 * Method: stop_listening_for_message
 * ----------------------------
 *   Stops the listen_for_message_Threads
 */
void btwebAPI::stop_listening_for_message()
{
	m_listening_for_message = false;
}

/*
 * Method: read_message
 * ----------------------------
 *   Returns the last sent message
 *
 *   returns: last message
 */
std::string btwebAPI::read_message()
{
	return strIn;
}

/*
 * Method: listen_for_connection_Thread
 * ----------------------------
 *   Thread that is listening for incomming connections
 */
void btwebAPI::listen_for_connection_Thread()
{
	
	int ilen;
	bool iSetOption = 1;

		GUID nguiD = { 00000000 - 0000 - 0000 - 0000 - 000000000000 };

		//The WSAStartup function initiates use of the Winsock DLL by a process
		if (WSAStartup(MAKEWORD(2, 2), &wsd) != 0)
		{
			fire_Error("listen_for_connection_Thread", "Unable to WSAStartup!");
			return;

		}

		// Creates Socket for connections
		s = socket(AF_BTH, SOCK_STREAM, BTHPROTO_RFCOMM);

		if (s == INVALID_SOCKET)
		{
			fire_Error("listen_for_connection_Thread", "Socket creation failed");
			return;
		}
		// sets the Sockets Options to reuse the Address
		setsockopt(s, SOL_SOCKET, SO_REUSEADDR, (char*)&iSetOption, sizeof(iSetOption));
				
		memset(&sab, 0, sizeof(sab));
		sab.addressFamily = AF_BTH;
		sab.port = 30;

		// The bind function associates a local address with a socket
		if (0 != bind(s, (SOCKADDR *)&sab, sizeof(sab)))
		{
			closesocket(s);
			fire_Error("listen_for_connection_Thread", "bind() failed");
			return;
		}
		
		// The listen function places a socket in a state in which it is listening for an incoming connection
		if (listen(s, 5) != 0)
		{
			fire_Error("listen_for_connection_Thread", "listen() failed");
			return;
		}


	do
	{

		ilen = sizeof(sockaddrs[count_sockets]);
		setsockopt(sockets[count_sockets], SOL_SOCKET, SO_REUSEADDR, (char*)&iSetOption, sizeof(iSetOption));
		
		// The accept function permits an incoming connection attempt on a socket
		sockets[count_sockets] = accept(s, (SOCKADDR *)&sockaddrs[count_sockets], &ilen);

		if (sockets[count_sockets] == INVALID_SOCKET)
		{
			closesocket(s);
			closesocket(sockets[count_sockets]);
			WSACleanup();	//The WSACleanup function terminates use of the Winsock 2 DLL (Ws2_32.dll)
			return;
		}
		
		fire_connectedDevice(getMAC(sockaddrs[count_sockets]), getName(getMAC(sockaddrs[count_sockets])));
		count_sockets++;

	} while (true);
}

/*
 * Method: listen_for_message_Thread
 * ----------------------------
 *   Threat that is listening for incomming messages
 *
 *	 x: Number of the thread
 */
void btwebAPI::listen_for_message_Thread(int x)
{
	m_listening_for_message = true;
	int iResult2;

	char recvbuf2[DEFAULT_BUFLEN] = "";

	int recvbuflen2 = DEFAULT_BUFLEN;

	std::string strIn2;
	do {
			// he recv function receives data from a connected socket or a bound connectionless socket
			iResult2 = recv(sockets[x], recvbuf2, recvbuflen2, 0);

			if (iResult2 > 0)
			{
				strIn2 = "";
				for (int i = 1; i <= iResult2; i++)
				{
					strIn2 += recvbuf2[i - 1];
				}
				fire_receivedMessage(getMAC(sockaddrs[x]), getName(getMAC(sockaddrs[x])), strIn2);
			}
		
	} while (m_listening_for_message);
	return;
}

/*
 * Method: LPWSTR2String
 * ----------------------------
 *   Converts LPW String to Char String
 *
 *	 outString: Address of destiny String
 *	 inLPWSTR: String for converting
 *	 codepage: Codepage for converting
 *
 *   returns: boolean value for error handling
 */
bool btwebAPI::LPWSTR2String(std::string& outString, const LPWSTR inLPWSTR, UINT codepage)
{
	bool retCode = false;
	char* temp = 0;
	int bytesRequired;
	bytesRequired = WideCharToMultiByte(codepage, 0, inLPWSTR, -1, 0, 0, 0, 0);
	if (bytesRequired > 0)
	{
		temp = new char[bytesRequired];
		int rc = WideCharToMultiByte(codepage, 0, inLPWSTR, -1, temp, bytesRequired, 0, 0);
		if (rc != 0)
		{
			temp[bytesRequired - 1] = 0;
			outString = temp;
			retCode = true;
		}
	}
	delete[] temp;
	return retCode;
}

/*
 * Method: getMAC
 * ----------------------------
 *   extracts MAC Address from Bluetooth Socket Address as String
 *
 *	 sab: Bluetooth Socket Address
 *
 *   returns: formated MAC Address
 */
std::string btwebAPI::getMAC(SOCKADDR_BTH sab){
	std::stringstream stream;
	std::stringstream stream2;

	stream << std::uppercase << std::hex <<  GET_NAP(sab.btAddr) << std::hex << GET_SAP(sab.btAddr);
	if (stream.str().size()==12)
		stream2 << stream.str()[0] << stream.str()[1] << ":" << stream.str()[2] << stream.str()[3] << ":" << stream.str()[4] << stream.str()[5] << ":" << stream.str()[6] << stream.str()[7] << ":" << stream.str()[8] << stream.str()[9] << ":" << stream.str()[10] << stream.str()[11];
	else
		stream2 << "0" << stream.str()[0] << ":" << stream.str()[1] << stream.str()[2] << ":" << stream.str()[3] << stream.str()[4] << ":" << stream.str()[5] << stream.str()[6] << ":" << stream.str()[7] << stream.str()[8] << ":" << stream.str()[9] << stream.str()[10];
	return stream2.str();
}

/*
 * Method: getName
 * ----------------------------
 *   Searches for the Device Name of a given MAC Address
 *
 *   returns: Device Name as String
 */
std::string btwebAPI::getName(std::string MAC){
	m_stopDiscovery = false;

	std::vector<ULONGLONG> foundDevices;

	if (WSAStartup(MAKEWORD(2, 2), &wsd) != 0)
	{
		fire_Error("startDiscovery_thread", "WSAStartup fails");
		return "Unknown";
	}

	WSAQUERYSET wsaq;
	HANDLE hLookup;
	LPWSAQUERYSET pwsaResults;
	DWORD dwSize;
	BOOL bHaveName;
	BT_ADDR btAddr;
	DWORD dwNameSpace;
	pwsaResults = (LPWSAQUERYSET)butuh.buf;
	dwSize = sizeof(butuh.buf);
	ZeroMemory(&wsaq, sizeof(wsaq));
	wsaq.dwSize = sizeof(wsaq);
	wsaq.dwNameSpace = NS_BTH;
	wsaq.lpcsaBuffer = NULL;

	// initialize searching procedure

	for (;;)
	{
		/*The WSALookupServiceBegin function initiates a client query that is constrained 
		by the information contained within a WSAQUERYSET structure. WSALookupServiceBegin 
		only returns a handle, which should be used by subsequent calls to 
		WSALookupServiceNext to get the actual results*/
		if (WSALookupServiceBegin(&wsaq, LUP_CONTAINERS, &hLookup) == SOCKET_ERROR)
		{
			continue;
		}

		ZeroMemory(pwsaResults, sizeof(WSAQUERYSET));
		pwsaResults->dwSize = sizeof(WSAQUERYSET);
		pwsaResults->dwNameSpace = NS_BTH;
		pwsaResults->lpBlob = NULL;

		/* The WSALookupServiceNext function is called after obtaining a handle from a 
		previous call to WSALookupServiceBegin in order to retrieve the requested 
		service information. */
		while (WSALookupServiceNext(hLookup, LUP_RETURN_NAME | LUP_RETURN_ADDR, &dwSize, pwsaResults) == 0)
		{

			btAddr = ((SOCKADDR_BTH *)pwsaResults->lpcsaBuffer->RemoteAddr.lpSockaddr)->btAddr;

			if (std::find(foundDevices.begin(), foundDevices.end(), btAddr) != foundDevices.end())
				continue;
			else
				foundDevices.push_back(btAddr);


			bHaveName = (pwsaResults->lpszServiceInstanceName) && *(pwsaResults->lpszServiceInstanceName);
			dwNameSpace = pwsaResults->dwNameSpace;

			// retrieve remote bt address from results
			CSADDR_INFO *pCSAddr;
			pCSAddr = (CSADDR_INFO *)pwsaResults->lpcsaBuffer;
			char addressAsString[2000];
			DWORD addressSize;
			addressSize = sizeof(addressAsString);

			WSAAddressToString(pCSAddr->RemoteAddr.lpSockaddr, pCSAddr->RemoteAddr.iSockaddrLength, NULL, (LPWSTR)addressAsString, &addressSize);
			std::string macAddress(addressAsString, addressSize);

			// fix: WSAAddressToString returns a '\0' after every character, so we strip them from string
			for (int i = 0; i < addressSize / 2; ++i)
				macAddress.erase(std::find(macAddress.begin(), macAddress.end(), '\0'));

			// fix: Strip brackets from string
			macAddress.erase(std::find(macAddress.begin(), macAddress.end(), '('));
			macAddress.erase(std::find(macAddress.begin(), macAddress.end(), ')'));


			//results[macAddress] = pwsaResults->lpszServiceInstanceName;
			std::string displayName;
			LPWSTR2String(displayName, pwsaResults->lpszServiceInstanceName);
			if (!macAddress.compare(MAC))
			{
				return displayName;
				/* The WSALookupServiceEnd function is called to free the handle 
				after previous calls to WSALookupServiceBegin and WSALookupServiceNext */
				WSALookupServiceEnd(hLookup);
			}
		}
		return "Unknown Device";

	}
}