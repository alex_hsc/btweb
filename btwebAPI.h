/**********************************************************\

Auto-generated btwebAPI.h

\**********************************************************/

#include <string>
#include <sstream>
#include <boost/weak_ptr.hpp>
#include "JSAPIAuto.h"
#include "BrowserHost.h"
#include "btweb.h"
#include <winsock2.h>
#include <memory>
#include <ws2bth.h>
#include <BluetoothAPIs.h>
#include <stdio.h>
#include <atlbase.h>
#include <atlconv.h>
#pragma comment(lib, "Ws2_32.lib")


typedef ULONGLONG bt_addr, *pbt_addr, BT_ADDR, *PBT_ADDR;

#ifndef H_btwebAPI
#define H_btwebAPI

class btwebAPI : public FB::JSAPIAuto
{
public:
	////////////////////////////////////////////////////////////////////////////
	/// @fn btwebAPI::btwebAPI(const btwebPtr& plugin, const FB::BrowserHostPtr host)
	///
	/// @brief  Constructor for your JSAPI object.
	///         You should register your methods, properties, and events
	///         that should be accessible to Javascript from here.
	///
	/// @see FB::JSAPIAuto::registerMethod
	/// @see FB::JSAPIAuto::registerProperty
	/// @see FB::JSAPIAuto::registerEvent
	////////////////////////////////////////////////////////////////////////////
	btwebAPI(const btwebPtr& plugin, const FB::BrowserHostPtr& host) :
		m_plugin(plugin), m_host(host)
	{
		registerMethod("connect_to_device", make_method(this, &btwebAPI::connect_to_device));
		registerMethod("send_message_to_device", make_method(this, &btwebAPI::send_message_to_device));
		registerMethod("startDiscovery", make_method(this, &btwebAPI::startDiscovery));
		registerMethod("stopDiscovery", make_method(this, &btwebAPI::stopDiscovery));
		registerMethod("disconnect_from_device", make_method(this, &btwebAPI::disconnect_from_device));
		registerMethod("start_listening_for_connection", make_method(this, &btwebAPI::start_listening_for_connection));
		registerMethod("start_listening_for_message", make_method(this, &btwebAPI::start_listening_for_message));
		registerMethod("stop_listening_for_connection", make_method(this, &btwebAPI::stop_listening_for_connection));
		registerMethod("stop_listening_for_message", make_method(this, &btwebAPI::stop_listening_for_message));
	}

	///////////////////////////////////////////////////////////////////////////////
	/// @fn btwebAPI::~btwebAPI()
	///
	/// @brief  Destructor.  Remember that this object will not be released until
	///         the browser is done with it; this will almost definitely be after
	///         the plugin is released.
	///////////////////////////////////////////////////////////////////////////////
	virtual ~btwebAPI() {};

	btwebPtr getPlugin();


	std::string btwebAPI::read_message();
	
	std::string getMAC(SOCKADDR_BTH sab);
	
	std::string btwebAPI::getName(std::string MAC);
	
	int send_message_to_device(std::string message);
	
	void connect_to_device(std::string macAddress, std::string deviceName);
	void btwebAPI::connect_to_device_Thread(std::string macAddress, std::string deviceName);
	int disconnect_from_device();
	
	bool btwebAPI::startDiscovery();
	void btwebAPI::startDiscovery_thread();
	void btwebAPI::stopDiscovery();

	void btwebAPI::start_listening_for_connection();
	void btwebAPI::listen_for_connection_Thread();
	void btwebAPI::stop_listening_for_connection();

	void btwebAPI::start_listening_for_message();
	void btwebAPI::listen_for_message_Thread(int x);
	void btwebAPI::stop_listening_for_message();


	// Event helpers
	FB_JSAPI_EVENT(test, 0, ());
	FB_JSAPI_EVENT(echo, 2, (const FB::variant&, const int));
	FB_JSAPI_EVENT(newDevice, 2, (const std::string&, const std::string&));
	FB_JSAPI_EVENT(connectionState, 1, (const int));
	FB_JSAPI_EVENT(connectedDevice, 2, (const std::string&, const std::string&));
	FB_JSAPI_EVENT(receivedMessage, 3, (const std::string&, const std::string&, const std::string&));
	FB_JSAPI_EVENT(Error, 2, (const std::string&, const std::string&));


private:
	btwebWeakPtr m_plugin;
	FB::BrowserHostPtr m_host;
	boost::thread* m_DiscoveryThread;
	bool m_stopDiscovery;
	bool m_listening_for_connection;
	bool m_listening_for_message;
	bool btwebAPI::LPWSTR2String(std::string& outString, const LPWSTR inLPWSTR, UINT codepage = CP_ACP);

};

#endif // H_btwebAPI

